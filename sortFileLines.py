# sorts the content of a file based on lines order
import argparse, pathlib, sys
parser = argparse.ArgumentParser(
                formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                description='This program takes a file and sorts its line, removing duplicate lines.\
                    Meant to be used for my package sorting and redundancy removal.')

def main():
    global parser
    parser.add_argument('-p', '--path',
                dest="path",
                help="Used to specify the file which will be loaded and sorted.",
                default = None,
                type=pathlib.Path)
    arg = parser.parse_args()
    if arg.path == None:
        print("You must specify a file to sort")
        parser.print_help()
        sys.exit(1)
    elif not arg.path.is_file():
        print("There seems to be a problem with the specified file.")
        sys.exit(1)
    else:
        data = sorted(list(set([lines.strip('\n') for lines in open(arg.path,'r').readlines()])))
        with open(arg.path, 'w') as f:
            for line in data:
                f.write("%s\n" % line)
        print("Files sorted.")
if __name__ == "__main__":
    main()
