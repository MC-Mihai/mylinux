#!/usr/bin/bash

# stop the screen from auto-rotating
#gsettings set org.gnome.settings-daemon.peripherals.touchscreen orientation-lock true

cp -r ./../images/* ~/Pictures 

gsettings set org.gnome.desktop.background picture-uri file:///$HOME/Pictures/system/background
gsettings set org.gnome.desktop.screensaver picture-uri file:///$HOME/Pictures/system/foreground

sudo sed -i "/Icon=./c\Icon=~\/Pictures\/system\/user" /var/lib/AccountsService/users/$USER
