#!/usr/bin/bash

# Created by Mihai (https://gitlab.com/Mihai-MCW) and licensed under GPLv2

# create the template files for the context menu entry 'new document'

touch ~/Templates/file
touch ~/Templates/text.txt
echo '#!/usr/bin/bash' > ~/Templates/bash.sh

# need to add wps and libre templates later

