#!/usr/bin/bash

# Created by Mihai (https://gitlab.com/Mihai-MCW) and licensed under GPLv2

hostnamectl set-hostname LLY14-F31

cd scripts

sh ./input.sh
sh ./GUI.sh
sh ./theme.sh
sh ./templates.sh
sh ./repos.sh
sh ./terminal.sh
sh ./fonts.sh
sh ./audio.sh
#sh ./git.sh # should start manually at the end because I need password generators

sudo dnf update -y
sudo dnf makecache

flatpak install skype
flatpak install wps

sudo dnf install $(cat ./packages/basic) -y

# The following packages are broken in fedora 31
# bmake iostat https://mega.nz/linux/MEGAsync/Fedora_29/x86_64/megasync-Fedora_29.x86_64.rpm


