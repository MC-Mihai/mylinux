#!/usr/bin/bash

# Created by Mihai (https://gitlab.com/Mihai-MCW) and licensed under GPLv2

# Noise cancellationpulse
sudo cp /etc/pulse/default.pa /etc/pulse/default.pa.bak
sudo bash -c 'echo -e "\n# Noise cancellation fix\nload-module module-echo-cancel source_name=noechosource sink_name=noechosink\nset-default-source noechosource\nset-default-sink noechosink" >> /etc/pulse/default.pa'
pulseaudio -k

#sudo dnf install alsa-tools pavucontrol -y
#https://bbs.archlinux.org/viewtopic.php?id=220962
#pactl set-sink-volume alsa_output.pci-0000_00_1b.0.analog-stereo 80%
