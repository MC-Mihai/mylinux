#!/usr/bin/bash

# Created by Mihai (https://gitlab.com/Mihai-MCW) and licensed under GPLv2

# activate tap to click
gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true

# planning to switch to libinput eventually and that code will be put here
