#!/usr/bin/bash

# Created by Mihai (https://gitlab.com/Mihai-MCW) and licensed under GPLv2

# ensure all 3 buttons for minimizing, maximizing and closing the window are show in gnome
gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'
gsettings set org.gnome.desktop.interface show-battery-percentage true

# makes the date appear
gsettings set org.gnome.desktop.interface clock-show-date true

# stop the screen from auto-rotating
gsettings set org.gnome.settings-daemon.peripherals.touchscreen orientation-lock true
# this one doesn't seem to work anymore
# gsettings set org.gnome.settings-daemon.plugins.orientation active false

# set the background and foreground the ones I use
# Note that my background is made by me and is subject to copyright
# The foreground and user logo might be subject to copyright too, but it's not mine
cp -r ../../../images/* ~/Pictures 
sudo cp ~/Pictures/icon/user /var/lib/AccountsService/icons/$USER
gsettings set org.gnome.desktop.background picture-uri file:///$HOME/Pictures/background
gsettings set org.gnome.desktop.screensaver picture-uri file:///$HOME/Pictures/foreground

# set the profile picture to the one I want
# doesn't work yet
sudo sed -i '/^Icon=/d' /var/lib/AccountsService/users/$USER
sudo sed -i -e '$aIcon=/var/lib/AccountsService/icons/'"$USER"'' /var/lib/AccountsService/users/$USER

# calendar set first day of week to monday
sudo localectl set-locale LC_MESSAGES=en_US.UTF-8 LANG=en_US.utf8 LC_NUMERIC=ro_RO.utf8 LC_TIME=fr_FR.utf8 LC_MONETARY=fr_FR.utf8 LC_PAPER=fr_FR.utf8 LC_MEASUREMENT=ro_RO.utf8 LC_ADDRESS=fr_FR.UTF-8 LC_TELEPHONE=fr_FR.UTF-8

# scraps
#sudo sed -i '8i8 Icon=/var/lib/AccountsService/icons/$USER' /var/lib/AccountsService/users/$USER
#sudo bash -c 'echo -e "Icon=/var/lib/AccountsService/icons/$USER" >> /var/lib/AccountsService/users/$USER' -  issue gives root user
#sudo sed -i -e '$aIcon=/var/lib/AccountsService/icons/\$USER' /var/lib/AccountsService/users/$USER
#sudo sed -i '/Icon=/c\Icon=/var/lib/AccountsService/icons/$USER' /var/lib/AccountsService/users/$USER


