#!/usr/bin/bash

# Created by Mihai (https://gitlab.com/Mihai-MCW) and licensed under GPLv2

#Disable Wayland and use Xorg
#sudo cp /etc/gdm/custom.conf /etc/gdm/custom.conf.backup

# back up the file
yes | sudo cp --backup=t /etc/gdm/custom.conf /etc/gdm/custom.conf.backup

# disable wayland
sudo sed -i '/WaylandEnable/s/^#//g' /etc/gdm/custom.conf

