#!/usr/bin/bash

# Created by Mihai (https://gitlab.com/Mihai-MCW) and licensed under GPLv2

# create the a RSA key, made specifically for git
# first get the email
echo Generating the RSA keys
read -p 'Email: ' email
mkdir -p ~/.ssh

cd ~/.ssh
#pushd ~/.ssh #alternative - pushes a path and accesses it

# walk through the shown steps from the command
ssh-keygen -t rsa -b 4096 -C "$email"
ls # show the ssh keys
cd -
#popd #alternative - going back to the previous path

# add the created key
eval "$(ssh-agent -s)"
read -p 'The file name used when you saved your private key: ' key
ssh-add ~/.ssh/$key

