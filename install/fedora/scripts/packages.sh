#!/usr/bin/bash

# Created by Mihai (https://gitlab.com/Mihai-MCW) and licensed under GPLv2

# batch install all the packages mentioned in the respective file
sudo dnf install $(cat ./packages/basic) -y
sudo dnf install $(cat ./packages/ARM) -y
sudo dnf install $(cat ./packages/creative) -y
sudo dnf install $(cat ./packages/extensions) -y
sudo dnf install $(cat ./packages/hacking) -y
sudo dnf install $(cat ./packages/misc) -y
sudo dnf install $(cat ./packages/python) -y
