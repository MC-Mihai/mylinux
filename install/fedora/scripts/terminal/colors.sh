#!/usr/bin/env bash

# Config for the the GNOME terminal color scheme theme #
export COLOR_01="#FF1B2B"	# HOST
export COLOR_02="#00D021"	# SYNTAX_STRING
export COLOR_03="#D79921"	# COMMAND
export COLOR_04="#747675"	# COMMAND_COLOR2
export COLOR_05="#FF30A2"	# PATH
export COLOR_06="#18A0B3"	# SYNTAX_VAR
export COLOR_07="#DD5436"	# PROMP
export COLOR_08="#928374"	#

export COLOR_09="#FF192E"	#
export COLOR_10="#67D972"	# COMMAND_ERROR
export COLOR_11="#FAE62F"	# EXEC
export COLOR_12="#9E9E9E"	#
export COLOR_13="#FF77C2"	# FOLDER
export COLOR_14="#6CD7E8"	#
export COLOR_15="#FF7846"	#
export COLOR_16="#666666"	#

export BACKGROUND_COLOR="#EEEEEE"	# Background Color
export FOREGROUND_COLOR="#1D2021"	# Text
export CURSOR_COLOR="$FOREGROUND_COLOR"
export PROFILE_NAME="Custom"
