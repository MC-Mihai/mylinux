#!/usr/bin/bash

# Created by Mihai (https://gitlab.com/Mihai-MCW) and licensed under GPLv2

# I chose PostgreSQL because from my research it seemed more reliable than other free SQL databases
# for my purposes
# More info about installing and configuring PostgreSLQ at
# https://fedoraproject.org/wiki/PostgreSQL

#Install the postgreSQL database
sudo dnf install $(cat ../packages/postgresql) -y

#Setup the postgresql database
sudo postgresql-setup initdb
#alternative to setup
#sudo postgresql-setup --initdb --unit postgresql

#Enable postgre to start at logon
#sudo systemctl enable postgresql

#Start postgresql
#sudo systemctl start postgresql
