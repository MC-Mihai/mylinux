#!/usr/bin/bash

# Created by Mihai (https://gitlab.com/Mihai-MCW) and licensed under GPLv2

# Install android dependencies
sudo dnf install $(cat ../packages/android) -y

# Download android studio
mkdir ./tmp

wget https://dl.google.com/dl/android/studio/ide-zips/3.4.1.0/android-studio-ide-183.5522156-linux.tar.gz -O ./tmp/androidstudio.tar.gz

# Extract the files in the install folder
tar -xvzf ./tmp/androidstudio.tar.gz -C ~/Software

# Clean temporary files used to install
rm -rf ./tmp

# Make an app menu entry
cat ./android/android-studio.desktop >> ~/.local/share/applications/android-studio.desktop

# Initialize / Run the app
sh ~/Software/android-studio/bin/studio.sh



